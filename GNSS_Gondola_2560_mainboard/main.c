/*
 * main.c
 *
 *  Created on: 22-12-2013
 *      Author: Dann
 */


#include <avr/io.h> // standardowe wej/wyj
#include <util/delay.h> // op�nienia
#include <string.h> // operacje na stringach
#include <avr/interrupt.h> // przerwania
#include <stdlib.h> // zawiera funkcje itoa()
#include <avr/wdt.h> // watchdog
#include "MYUART/myuart.h"
//#include "TWI/mytwi.h" // I2C
#include <avr/power.h> // makra do oszcz. energii
//#include "math.h"

#define  identyfikator_urzadzenia "G2"
#define  wersja_softu "V19112016t"

#define  LED_on PORTD |= (1<<7);
#define  LED_off PORTD &= ~(1<<7);
#define uart1_interrupt_enable UCSR1B |= (1<<RXCIE1)   // eksperyment
#define uart1_interrupt_disable UCSR1B &= ~(1<<RXCIE1)
#define uart1_RX_enable UCSR1B |= (1<<RXEN1)  
#define uart1_RX_disable UCSR1B &= ~(1<<RXEN1)
#define eksperyment_pozwolenie_ON PORTD |= _BV(5)
#define eksperyment_pozwolenie_OFF PORTD &= ~_BV(5)
#define uart2_interrupt_enable UCSR2B |= (1<<RXCIE2)
#define uart2_interrupt_disable UCSR2B &= ~(1<<RXCIE2)
#define uart3_interrupt_enable UCSR3B |= (1<<RXCIE3)
#define uart3_interrupt_disable UCSR3B &= ~(1<<RXCIE3)
#define Liczba_Ramek_GPS 2 
#define Pierwsza_Ramka_RMC String[0][3] == 'R' && String[0][4] == 'M' && String[0][5] == 'C'
#define Pierwsza_Ramka_VTG String[0][3] == 'V' && String[0][4] == 'T' && String[0][5] == 'G'

volatile static uint16_t licznik = 0;
volatile static uint16_t prog_timer_sek = 0;
volatile static uint8_t prog_timer_sek_1 = 0;
volatile static uint8_t prog_timer_sek_2 = 0;
volatile static uint16_t milisek = 0;
//volatile static uint16_t delay_timer = 0;
volatile static uint8_t flaga_modem_rx = 0;
volatile static uint8_t flaga_eksperyment_rx = 0;
volatile static uint8_t flaga_radio_rx = 0;
volatile static uint8_t flaga_gps_rx = 0;
volatile static uint8_t flaga_czysc_buf_exp = 0;
uint16_t modem_sekund = 5;
uint16_t mors_sekund = 300;
volatile char String[Liczba_Ramek_GPS+1][100];  // gps
char String2[45]; // modem
char String3[96]; // eksperyment
char String4[21]; // radio
char Pozycja_Decym[20];
char LastWysokosc[8] = {"0\0\0\0\0\0\0\0"}; // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
char Wznoszenie[4]; // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
volatile static uint8_t NrZnaku = 0;
volatile static uint8_t NrWiersza = 0;
volatile static uint8_t inc2 = 0;
volatile static uint8_t inc3 = 0;
volatile static uint8_t inc4 = 0;
volatile static char Fix[2];
char Splited_GGA[16][20];
char Splited_VTG[12][15];
volatile static char LastRamkaCzasUTC[7];
volatile static char LastRamkaSzerokosc[20];
volatile static char LastRamkaDlugosc[20];
volatile static char LastRamkaWysokosc[10];
volatile static char LastRamkaPredkosc[15];
volatile static char LastRamkaFix[2];
volatile static char LastRamkaSatelity[3];
char Temp_wewn_str[6];
char Temp_zewn_str[6];
char bufor[10];
uint16_t LicznikRamek = 1;
volatile static char IO_IN_OUT[7]; // {WE1, WE2, WY1, WY2}
volatile static uint8_t ExperimentAutoRequest = 0xFF; // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
char Napiecie_string[8];

void RESET();
void RESET_Watchdog();
uint16_t adc_pomiar ( uint8_t kanal);
void dane_uart();
static inline void IntToString(int liczba);
void LM335_pomiar();
void napiecie_pomiar(); //
void Ogarnij_IO();
void Pikacz_telemetria();
void odbierz_eksperyment();
void ogarnij_GNSS();
void czysc_buf_GPS();
void dane_uart_LAST_OK();

void Config_Ublox();

void init()
{
wdt_enable(WDTO_4S);  // od razu rekonfiguruje watchdoga'a na 4s i go resetuje dla pewnosci
wdt_reset();

DDRA =  0b11111111;	  
DDRB =  0b11111111;    
DDRC =  0b11111111;    
DDRD =  0b11101111;    // PD4 wejscie flow control uart 1
DDRE =	0b11111011;	   // PE2 wejscie flow control uart 0
DDRF =	0xFF;
DDRG =	0xFF;
DDRH =	0b10001011;	   // PH4-6 wejscia IO, PH2 wejscie flow control uart 2
DDRJ =	0b11111011;	   // PJ2 wejscie flow control uart 3
DDRK =	0b00000111;	   // <<ADC>> uzywane kanaly jako wejcia!! - nieuzywane jako wyjscia  
DDRL =	0b11111111;	   // PL3-5 wyjscia IO

PORTA =  0b00000000; 
PORTB =  0b00000000;    // port B pocz. zera
PORTC =  0b00000000;    
PORTD =  0b00000000;    // port D pocz. zera
PORTE =	 0x00;
PORTF =	 0x00;
PORTG =	 0x00;
PORTH =	 0x00;
PORTJ =	 0x00;
PORTK &= ~0b00000111;	// zera to aktywne kanaly ADC  - reszta podciagana do zera
PORTL =	 0x00;

// inicjalizacja ADC
ADMUX  = (1<<REFS1) | (1<<REFS0);	// Internal 2.56V Voltage Reference
ADCSRA = (1<<ADEN) | (1<<ADPS2) | (1<<ADPS1) | (1<<ADPS0);	//enable + konwersja prescaler 128
DIDR0  = 0xFF; // wszystkie adc z odlaczonym rejestrem PIN - oszczedzanie energii
DIDR2  = 0xFF; // j.w.

// inicjalizacja UART'u
uart0_init(__UBRR_0);		
uart1_init(__UBRR_1);
uart2_init(__UBRR_2);
uart3_init(__UBRR_3);

// TIMER2 W TRYBIE CTC
OCR2A = 16;  // 16384000/1024=16000 >> 16000/16=1000Hz
TCCR2A |= (1<<WGM21); //tryb ctc
TCCR2B |= (1<<CS22) | (1<<CS21) | (1<<CS20);    // preskaler TIMER2 1024
TIMSK2 |= (1<<OCIE2A);

sei(); // globalne wlaczenie przerwan (cli() - wy��cza)

/*
*    systemowe bip bip
*/

_delay_ms(1000);
uart2_puts(wersja_softu);
uart2_puts(">> INIT OK <<");
//uart2_puts_noCR(" \r\n");
prog_timer_sek=0;
}

ISR(USART0_RX_vect)  // OBSLUGA GPS >> UART0
{
uart2_interrupt_disable; // blokuje przerwania od portu xbee   <<<<<<<<<<<<<

	unsigned char tmp = UDR0; // przepisz  znak z rejestru do zmiennej 
	
	if (NrWiersza < Liczba_Ramek_GPS)								// <= 6
	{		
		if (tmp != 10)							// przepisuj dopoki nie trafisz na <LF>
		{
			String[NrWiersza][NrZnaku] = tmp;		// jesli to nie znak konca linii to przepisz znak do obecnej pozycji i zwieksz licznik znakow
			NrZnaku++;
		}
		else  // gdy znak konca linii
		{
			if( Pierwsza_Ramka_VTG ) // gdy pierwszy wiersz zawiera pierwsza ramke z serii od GPS mozna przejsc do nastepnego wiersza bo beda w kolejnosci
			{
				 NrWiersza++; 
				 NrZnaku = 0;
			} 
			else 
			{ 
				NrWiersza = 0; 
				NrZnaku = 0;
			}						// w przeciwnym wypadku nadpisuj pierwszy wiersz az do uzyskania synchronizacji
		} 				
	}
	
	if (NrWiersza >= Liczba_Ramek_GPS) // sygnalizacja zakonczenia i wystawienie flagi
	{
		NrWiersza = 0; 
		NrZnaku = 0; 
		flaga_gps_rx = 0xFF;
	}
													
uart2_interrupt_enable; // odblokowuje przerwania od portu xbee    <<<<<<<<<<<<
}

ISR(USART2_RX_vect)  // odbior komend z ziemii - modem
{
	unsigned char tmp2 = UDR2;
	if (inc2 > 40) {inc2 = 0; flaga_modem_rx = 1;} // gdy przepelni bufor zeruje licznik znakow i przechodzi do analizy bufora (zakonczy sie ona bledem ale wyczysci bufor !!)
	if ( (tmp2 == '>' && inc2 == 0) || (String2[0] == '>' && tmp2 != 'U') )  //  <<<<<<<<<<<<<<<<<<
	{
	if (tmp2 != 13) { String2[inc2] = tmp2; inc2++; } // przepisz znaki do stringa dop�ki nie jest to <CR>
	else flaga_modem_rx = 1;						  // je�li jest, czyli sko�czy� zape�nia� string, to wywal flag�
	}
}

ISR(USART1_RX_vect) // odbior danych od eksperymentu
{
	unsigned char tmp3 = UDR1;
	if (inc3 >= 86) inc3 = 0;
	if ( (tmp3 == '>' && inc3 == 0) || (String3[0] == '>') )  //  
	{
		if (tmp3 != 13) { String3[inc3] = tmp3; inc3++; } // przepisz znaki do stringa dop�ki nie jest to <CR>
		else flaga_eksperyment_rx = 1;						  // je�li jest, czyli sko�czy� zape�nia� string, to wywal flag�
	}
}

ISR(USART3_RX_vect) // odbior danych od radia
{
	unsigned char tmp4 = UDR3;
	if (inc4 >= 15) {inc4 = 0; flaga_radio_rx = 1;} // gdy przepelni bufor zeruje licznik znakow i przechodzi do analizy bufora (zakonczy sie ona bledem ale wyczysci bufor !!)
	if ( (tmp4 == '>' && inc4 == 0) || (String4[0] == '>') )  
	{
		if (tmp4 != 13) { String4[inc4] = tmp4; inc4++; } // przepisz znaki do stringa dop�ki nie jest to <CR>
		else flaga_radio_rx = 1;						  // je�li jest, czyli sko�czy� zape�nia� string, to wywal flag�
	}
}

ISR (TIMER2_COMPA_vect) // obsluga przerwania od TIMER2
{
  licznik++;      // zmienna liczaca 1000 przerwan dla 1 sekundy
  milisek++;
  //delay_timer++;

  if (licznik >= 1000)// fizyczna liczba przerwan ktore uplyna zanim wykonasz dzialanie  // w tym wypadku ~1s
  	 {
	  	prog_timer_sek++;
	  	prog_timer_sek_1++;
	  	prog_timer_sek_2++;
	  	licznik = 0;
  	 }
}

int main(void)
{
	init();  // inicjalizacja (wykonuje tylko raz)// 

	while(1) //****************************************************************************************************** GLOWNA PETLA PROGRAMU v
	{
		wdt_reset();

		if (flaga_gps_rx == 0xFF ) // obsluga rzeczy najwazniejszych, po odebraniu dnaych z GPS  <<<<<<<<
		{
			flaga_gps_rx = 0;		// skasuj flagi od wszystkich ramek
			ogarnij_GNSS();					
		}

//********************************************************************************************************************** KOMENDY Z ZIEMI v
		if (flaga_modem_rx == 1) // wykona to tylko gdy skompletowal komende odebrana z ziemi
	{
			uart2_interrupt_disable;  // blokuje odbieranie komend i innych znakow gdy trwa analiza poprzednich   

			flaga_modem_rx = 0;  // od razu kasuj flage na (wypadek niewykonania dalszej analizy)

			if ( strcmp(String2, ">TCW") == 0 ) // telemetria morse
			{
				//Pikacz_telemetria();
				prog_timer_sek = mors_sekund; // zeruj czas do wypikania telemetrii
			}
			
	   else if ( strcmp(String2, ">TXB") == 0 ) // telemetria modem // >>>>>>>>>>>>>>>>>>> niech bedzie tu wysylanie ostatniej poprawnej zamiast obecnej
	   	   {
				LM335_pomiar();
				napiecie_pomiar();
				odbierz_eksperyment();//<<<<<<<<<<<<<<<<<
				//dane_uart();
				dane_uart_LAST_OK();
				//czysc_buf_GPS();
	   	   }
			  
	   else if ( strcmp(String2, ">RST") == 0 ) { RESET(); } // reset programu przez skok do bootloadera
		   
	   else if (  strcmp(String2, ">RSTW") == 0  ) { RESET_Watchdog(); } // reset programu przez watchdog
			
		else if ( strcmp(String2, ">MSM=1") == 0 )
		{
			uart3_puts(">STM=1"); //<<<<<<<<<<<<<<<<<<<<<
		}
			
		else if ( strcmp(String2, ">MSM=0") == 0 )
		{
			uart3_puts(">STM=0"); //<<<<<<<<<<<<<<<<<<<<
		}
			
		else if ( strcmp(String2, ">WY1=1") == 0 ) // WYJSCIE IO_1 <ON>
		{
			PORTL |= _BV(5);
		}
		
		else if ( strcmp(String2, ">WY1=0") == 0 ) // WYJSCIE IO_1 <OFF>
		{
			PORTL &= ~_BV(5);
		}
		
		else if ( strcmp(String2, ">WY2=1") == 0 ) // WYJSCIE IO_2 <ON>
		{
			PORTL |= _BV(4);
		}
		
		else if ( strcmp(String2, ">WY2=0") == 0 ) // WYJSCIE IO_2 <OFF>
		{
			PORTL &= ~_BV(4);
		}
		
		else if ( strcmp(String2, ">WY3=1") == 0 ) // WYJSCIE IO_3 <ON>
		{
			PORTL |= _BV(3);
		}
		
		else if ( strcmp(String2, ">WY3=0") == 0 ) // WYJSCIE IO_3 <OFF>
		{
			PORTL &= ~_BV(3);
		}
		
		else if ( strcmp(String2, ">EAR=1") == 0 ) // tryb pobierania danych z eksperymentu odblokowany <<<<<
		{
			ExperimentAutoRequest = 0xFF;
			if (ExperimentAutoRequest == 0xFF) uart2_puts(">> EAR-ON <<");
		}
		
		else if ( strcmp(String2, ">EAR=0") == 0 ) // tryb pobierania danych z eksperymentu zablokowany <<<<<<<<
		{
			ExperimentAutoRequest = 0x00;
			if (ExperimentAutoRequest == 0x00) uart2_puts(">> EAR-OFF <<");
		}
		
		else if ( strstr(String2, ">RTR:") != 0 ) // PRZEKAZANIE DANYCH DO PIKACZA   /// gdy bufor zawiera dany ciag znakow
		{
			for (int i=0; String2[i]!=0; i++) // przepisuje [przesuwa] string by wywalic tresc komendy a zostawic parametry
			{
				String2[i]=String2[i+5];
			}
			uart3_puts_noCR(">TX:");
			uart3_puts(String2);
		}
		
		else if ( strstr(String2, ">CONFGPS") != 0 ) // na ��danie skonfiguruj modul gps (gdy jest nowy lub config jest w ramie)
		{
			Config_Ublox();
		}

	   else uart2_puts("ERROR");


		inc2=0;  // zeruj licznik znakow po wykonaniu komendy

		for (uint8_t i = 0; i<=45; i++)  // czysc bufor komend
			{
				String2[i] = 0;
			}

		uart2_interrupt_enable;	//	odblokowuje odbieranie komend i innych znakow po analizie    
	}
		
//************************************************************************************************************************ KONIEC KOMEND Z ZIEMI ^


//********************************************************************************************************************** KOMENDY OD RADIA v
		if (flaga_radio_rx == 1) // wykona to tylko gdy skompletowal komende 
		{
			uart3_interrupt_disable;  // blokuje odbieranie komend i innych znakow gdy trwa analiza poprzednich   
			flaga_radio_rx = 0;  // od razu kasuj flage na (wypadek niewykonania dalszej analizy)

			if ( strcmp(String4, ">D0") == 0 ) //  radio odebralo komende DTMF
			{
				
			}	
			else if ( strcmp(String4, ">D1") == 0 ) // radio odebralo komende DTMF
	   		{
				//Pikacz_telemetria();
				prog_timer_sek = mors_sekund; // zeruj czas do wypikania telemetrii
	   		}		  
			else if ( strcmp(String4, ">D2") == 0 ) //  radio odebralo komende DTMF
			{
				RESET_Watchdog();
			}	
			else if ( strcmp(String4, ">D3") == 0 ) //  radio odebralo komende DTMF
			{
				// potwierdzenie resetu pikacza
			}	
			else if ( strcmp(String4, ">D4") == 0 ) //  radio odebralo komende DTMF
			{		
			}	
			else if ( strcmp(String4, ">D5") == 0 ) //  radio odebralo komende DTMF
			{		
			}		
			else if ( strcmp(String4, ">D6") == 0 ) //  radio odebralo komende DTMF
			{		
			}		
			else if ( strcmp(String4, ">D7") == 0 ) //  radio odebralo komende DTMF
			{		
			}	
			else if ( strcmp(String4, ">D8") == 0 ) //  radio odebralo komende DTMF
			{		
			}
			else if ( strcmp(String4, ">D9") == 0 ) //  radio odebralo komende DTMF
			{		
			}
			else if ( strcmp(String4, ">R-ON") == 0 ) //  radio odebralo komende 
			{
				uart2_puts(">> RADIO ON <<");
			}
			else if ( strcmp(String4, ">R-OFF") == 0 ) //  radio odebralo komende
			{
				uart2_puts(">> RADIO OFF <<");
			}
			else if ( strcmp(String4, ">INIT OK") == 0 ) //  radio sie zainicjalizowalo
			{
				uart2_puts(">> RADIO INIT <<");
			}
			else if ( strcmp(String4, ">ST-ON") == 0 ) //  radio w trybie standalone
			{
				uart2_puts(">> RADIO ST-ON <<");
			}
			else if ( strcmp(String4, ">ST-OFF") == 0 ) //  radio wyszlo z trybu standalone
			{
				uart2_puts(">> RADIO ST-OFF <<");
			}
			//else uart3_puts(">ERROR");

			inc4=0;  // zeruj licznik znakow po wykonaniu komendy
			for (uint8_t i = 0; i <= 21; i++)  // czysc bufor komend
				{
					String4[i] = 0;
				}
			uart3_interrupt_enable;	//	odblokowuje odbieranie komend i innych znakow po analizie    
		}
		
//***************************************************************************************************************** KONIEC KOMEND OD RADIA ^

//***************************************************************************************************************** CZYNNOSCI ZALEZNE OD CZASU v
		if (prog_timer_sek_2 >= 1)	// wykonuje sie co 1s
		{
			PORTB ^= _BV(3);		// co 1s neguj portb.4 by zresetowac zewn. watchdog (ten sam do MISO)
			prog_timer_sek_2 = 0;

			if(Fix[0] == '2' || Fix[0] == '3')	// dodatkowo sprawdz czy jest fix
			 {
				modem_sekund = 2;				// gdy jest fix ustaw czestsze nadawanie
				mors_sekund = 150;
			 }
			else
			{
				modem_sekund = 10;				// przy kazdej innej wartosci fixa ustaw zadsze nadawanie
				mors_sekund = 600;
			}
			
			Ogarnij_IO();						
		}

		if (prog_timer_sek >= mors_sekund)   // TELEMETRIA MORSEM CO ~300s = 5min /////
		{
			prog_timer_sek = 0;
			Pikacz_telemetria();
		}

		if (prog_timer_sek_1 >= modem_sekund)    // TELEMETRIA UART CO ~2s jesli timer programowy zliczyl czas
		    {									
				prog_timer_sek_1 = 0;
				LM335_pomiar();
				napiecie_pomiar();
				odbierz_eksperyment();//<<<<<<<<
				dane_uart();
				czysc_buf_GPS();
		    }

		// koniec while'a /////////////////////////////////////////////////////////////////////////////////// KONIEC GLOWNEJ PETLI PROGRAMU
	}
}

void dane_uart()
{
	LED_on;
		uart2_putc('#');
		IntToString(LicznikRamek);
		uart2_puts_noCR( bufor ); // bufor licznika ramek
		uart2_putc( ';' ); 
		uart2_puts_noCR( Splited_GGA[1] ); // UTC
		uart2_putc( ';' ); 
		uart2_puts_noCR( Temp_wewn_str );
		uart2_putc( ';' ); 
		uart2_puts_noCR( Temp_zewn_str );
		uart2_putc( ';' ); 			
		uart2_puts_noCR( Napiecie_string );
		uart2_putc( ';' ); 
		uart2_puts_noCR( Fix );
	    uart2_putc( ';' );  
	
		if( Splited_GGA[7][0] != 0 ) uart2_puts_noCR( Splited_GGA[7] ); // Satelity
	    else uart2_puts_noCR( "??" );
		uart2_putc( ';' ); 

	    if(Fix[0] == '2' || Fix[0] == '3')
	    {
			if(Splited_GGA[9][0] == 0 && Splited_GGA[9][1] == 0) uart2_puts_noCR( "??" ); // 
			else uart2_puts_noCR( Splited_GGA[9] ); // wysokosc
			uart2_putc( ';' ); 		
			uart2_puts_noCR( Splited_GGA[2] ); // szerokosc geo
			uart2_putc( ';' ); 
			uart2_puts_noCR( Splited_GGA[4] ); // dlugosc geo
			uart2_putc( ';' ); 
			uart2_puts_noCR( Splited_VTG[7] );		//Predkosc !! teraz km/h - wczesniej wezly
			uart2_putc( ';' ); 
			uart2_puts_noCR( Wznoszenie ); // wznoszenie
			uart2_putc( ';' ); 
	    }
	    else 
		{
			uart2_puts_noCR( "??;" );
			uart2_puts_noCR( "??;" );
			uart2_puts_noCR( "??;" );
			uart2_puts_noCR( "??;" );
			uart2_puts_noCR( "??;" );
		}
			uart2_puts_noCR( IO_IN_OUT );
			uart2_putc( ';' );            // uart2_puts( " ; " ); <<<<<<<<
			
			uart2_puts_noCR( String3 ); // <<<<<<<<
			uart2_putc( ';' );
			
			uart2_puts_noCR( identyfikator_urzadzenia ); // <<<<<<<<
			uart2_puts( ";" );		// <<<<<<<<<
			
			LicznikRamek++;
		LED_off;	
}

void dane_uart_LAST_OK() // wysyla ramke z aktualnymi pomiarami ale z ostatnia poprawna pozycja
{
	LED_on;
	uart2_putc('#');
	IntToString(LicznikRamek);
	uart2_puts_noCR( bufor ); // bufor licznika ramek
	uart2_putc( ';' );
	uart2_puts_noCR( LastRamkaCzasUTC ); // UTC
	uart2_putc( ';' );
	uart2_puts_noCR( Temp_wewn_str );
	uart2_putc( ';' );
	uart2_puts_noCR( Temp_zewn_str );
	uart2_putc( ';' );
	uart2_puts_noCR( Napiecie_string );
	uart2_putc( ';' );
	uart2_puts_noCR( LastRamkaFix );
	uart2_putc( ';' );
	if( LastRamkaSatelity[0] != 0 ) uart2_puts_noCR( LastRamkaSatelity ); // Satelity
	else uart2_puts_noCR( "??" );
	uart2_putc( ';' );
	if(LastRamkaFix[0] == '2' || LastRamkaFix[0] == '3')
	{
		if(LastRamkaWysokosc[0] == 0 && LastRamkaWysokosc[1] == 0) uart2_puts_noCR( "??" ); //
		else uart2_puts_noCR( LastRamkaWysokosc ); // wysokosc
		uart2_putc( ';' );
		uart2_puts_noCR( LastRamkaSzerokosc ); // szerokosc geo
		uart2_putc( ';' );
		uart2_puts_noCR( LastRamkaDlugosc ); // dlugosc geo
		uart2_putc( ';' );
		uart2_puts_noCR( LastRamkaPredkosc );		//Predkosc !! teraz km/h - wczesniej wezly
		uart2_putc( ';' );
		uart2_puts_noCR( "" ); // wznoszenie
		uart2_putc( ';' );
	}
	else
	{
		uart2_puts_noCR( "??;" );
		uart2_puts_noCR( "??;" );
		uart2_puts_noCR( "??;" );
		uart2_puts_noCR( "??;" );
		uart2_puts_noCR( "??;" );
	}
	uart2_puts_noCR( IO_IN_OUT );
	uart2_putc( ';' );            // uart2_puts( " ; " ); <<<<<<<<
	uart2_puts_noCR( String3 ); // <<<<<<<<
	uart2_puts_noCR( "|LAST_OK|" );
	uart2_putc( ';' );		// <<<<<<<<<
	uart2_puts_noCR( identyfikator_urzadzenia ); // <<<<<<<<
	uart2_puts( ";" );		// <<<<<<<<<
	
	LicznikRamek++;
	LED_off;
}

void Pikacz_telemetria()
{
	napiecie_pomiar();
	LM335_pomiar();
	uart3_puts_noCR(">TX:TEL,");
	uart3_puts_noCR(LastRamkaSzerokosc); // szerokosc geo
	uart3_putc(',');
	uart3_puts_noCR(LastRamkaDlugosc); // dlugosc geo
	uart3_putc(',');
	uart3_puts_noCR(LastRamkaWysokosc); // wysokosc	
	uart3_putc(',');
	uart3_puts_noCR(Napiecie_string);
	uart3_putc(',');
	uart3_puts_noCR(Temp_wewn_str);
	uart3_putc(',');
	uart3_puts_noCR(Temp_zewn_str);
	
	if( Fix[0] == '1' )
	{
		uart3_putc(',');
		uart3_puts("NFIX");
	}

	else if( Fix[0] == 0 || Splited_GGA[2][0] == 0 ) // Splited_GGA[2][0] == 0 - chodzi o pusta szerokosc geo
	{
		uart3_putc(',');
		uart3_puts("NGPS");
	}
	else uart3_putc('\r');
}

void RESET() // funkcja resetowania CPU przez skok do poczatku pamieci bootloadera
{
	cli();				  // deaktywuje przerwania zewnetrzne
	_delay_ms(100); // poczekaj troszke
	asm("jmp 0x7800;");	  // skocz do poczatku pamieci bootloadera (przy 2kb pojemnosci >> 4rech?)
}

void RESET_Watchdog() // <<<<<<<<<<< !!!!!!!!!!!!!!!
{
	   _delay_ms(1000);
	   wdt_enable(WDTO_30MS);
	   cli();
	   while(1);
}

/*     wykonanie pomiaru ADC     */
uint16_t adc_pomiar ( uint8_t kanal)
{
	ADMUX &= ~0x07;	//clear last 3 bits (MUX4-0)
	ADCSRB &= ~(1<<MUX5); // clear first bit MUX5 (for adc selection)
	
	if (kanal >= 8)	// jesli kanal jest np 15 to do admux wpisze 7 (skalowanie na drugi bajt adc)
	{ 
		kanal -= 8; 
		ADCSRB |= (1<<MUX5); // ustaw mux5 zeby ostatecznie wyszedl kanal 15 (drugi bajt adc)
	}
	
	ADMUX |= (0x07 & kanal); //select channel
	ADCSRA |=(1<<ADSC);	//start konwersji

	while( !(ADCSRA & (1<<ADIF)) ); //czekaj na koniec konwersji
	//return ADCH;
	unsigned char x = ADCL;
	unsigned char y = ADCH;
	return (y<<8) + x;
}

static inline void IntToString(int liczba)
{
	itoa(liczba, bufor, 10);
}

void napiecie_pomiar()// <<<<<<<<<<<<<< #2
{
	for (uint8_t i = 0; i < 8; i++) Napiecie_string[i] = 0; // czysci stringa
		
	volatile static double Napiecie_cyfra = 0; // zeruje zmienna liczbowa
	
	for (int i=0; i < 50; i++) Napiecie_cyfra = Napiecie_cyfra + adc_pomiar(12);
	
	Napiecie_cyfra = (Napiecie_cyfra/50)* 0.01082615; // zmienne: pierwsza to mno�nik wynikajacy z dzielnika i konwersji adc, druga to kalibracyjna
	dtostrf(Napiecie_cyfra,0,3,Napiecie_string); 	  // podobno lepszy mnoznik: 0.01082615 zamiast 0.01134732 - 0.394	
}

void LM335_pomiar()
{
	volatile static int Temp_wewn = 0;
	volatile static int Temp_zewn = 0;

	for(int i=0; i<100; i++)
	{
		Temp_wewn = Temp_wewn + ((adc_pomiar(13) / 2) -81); // lm35D wewn
		Temp_zewn = Temp_zewn + ((adc_pomiar(14) / 2) -82); // lm35D zewn
	}
	
	Temp_wewn= Temp_wewn/100;
	Temp_zewn = Temp_zewn/100;
	itoa(Temp_wewn, Temp_wewn_str, 10);
	itoa(Temp_zewn, Temp_zewn_str, 10);
}

void Ogarnij_IO()
{
	if ((PINH & _BV(6)) == _BV(6)) {IO_IN_OUT[0] = '1';} // WE-1
		else {IO_IN_OUT[0] = '0';} 
			
	if ((PINL & _BV(5)) == _BV(5)) {IO_IN_OUT[1] = '1';} // WY-1
		else {IO_IN_OUT[1] = '0';} 
			
	if ((PINH & _BV(5)) == _BV(5)) {IO_IN_OUT[2] = '1';} // WE-2
		else {IO_IN_OUT[2] = '0';} 
			
	if ((PINL & _BV(4)) == _BV(4)) {IO_IN_OUT[3] = '1';} // WY-2
		else {IO_IN_OUT[3] = '0';} 
			
	if ((PINH & _BV(4)) == _BV(4)) {IO_IN_OUT[4] = '1';} // WE-3
	else {IO_IN_OUT[4] = '0';}
	
	if ((PINL & _BV(3)) == _BV(3)) {IO_IN_OUT[5] = '1';} // WY-3
	else {IO_IN_OUT[5] = '0';}
}

void odbierz_eksperyment()
{	
		if(String3[0] != 0 && String3[1] != 0 && String3[2] != 0 && String3[3] != 0 ) // czysci bufor jezeli jego pierwsze 4 znaki nie sa NULL (czyli teoretycznie bufor czysty)
		{
			for(uint8_t i = 0; i < 96; i++)  { String3[i] = 0; }
			flaga_czysc_buf_exp = 0;
		}

	if (ExperimentAutoRequest == 0xFF)
	{
		uart1_RX_enable;			// odblokuj fizycznie linie odbioracza
		uart1_interrupt_enable;		// odblokuj przerwanie do odbioru danych
		eksperyment_pozwolenie_ON;	// wlacz port PD5
		uart1_puts("!");			// wyslij rozkaz nadania nadych  !<CR>
		milisek=0;					// zresetuj timer wychodzacy z petli po 50ms
		while(1)
		{
			if (flaga_eksperyment_rx == 1 || milisek >= 50) // jesli odebral ramke lub skonczyl sie czas
			{
				flaga_eksperyment_rx = 0;		// wyzeruj flage odbioru
				inc3 = 0;
				break;							// wyjdz z petli while wczesniej niz zadziala timer
			}
		}
		eksperyment_pozwolenie_OFF;		// wylacz port PD5
		uart1_interrupt_disable;		// wylacz przerwanie
		uart1_RX_disable;				// zablokuj linie odbiorcza
	}	
}

void ogarnij_GNSS()
{	
	// sprawdzanie sum kontrolnych----------------------------------------------------------------------------------------------------------------------
	uint8_t flaga_gps_checksum[9] = "00000000\0";
	
	for (uint8_t a = 0; a < Liczba_Ramek_GPS; a++) // dla kazdego wiersza glownego bufora
	{
		uint8_t checksum = 0;
		uint8_t indeks = 0;
		char Suma_Z_Ramki[3] = {'0','\0','\0'}; // bez inicjalizacji konkretnymi wartosciami w dalszych polach byly losowe znaki!
		for(uint8_t i = 1; String[a][i] != '*'; i++)
		{		
			checksum ^= String[a][i]; indeks = i+1; // liczenie sumy kontrolnej (XOR)
		}
		Suma_Z_Ramki[0] = String[a][indeks+1];
		Suma_Z_Ramki[1] = String[a][indeks+2];
		uint8_t int_Suma_Z_Ramki = (uint8_t)strtol(Suma_Z_Ramki, NULL, 16);
		if(int_Suma_Z_Ramki == checksum) flaga_gps_checksum[a] = 0xFF;
		else flaga_gps_checksum[a] = 0x00;
		
		// inteligentne splitowanie po przecinkach--------------------------------------------------------------------------------------------------------------	
			
		char *p_start = 0, *p_end = 0; // wskazniki na komorki pamieci z przecinkiem od ktorego ma zaczynac przepisywanie i przeciniem gdzie ma konczyc
		char ii = 0; // licznik 'wierszy'
		p_start = (char*)String[a]; // ladowanie wskaznika w zaleznosci od ramki (VTG/GGA)
		
		if (String[a][3] == 'V' && String[a][4] == 'T' && String[a][5] == 'G' && flaga_gps_checksum[0] == 0xFF) // $GNVTG,,T,,M,0.031,N,0.058,K,D*37
		{	
				while(1)				
				{
					p_end = strchr(p_start, ','); // podaj wskaznik na komorke pamieci nastepnego przecinka, zacynajac szukanie od komorki pamieci z obecnym
					if (p_end) // jezeli znalazl nastepny przecinek (!=0)
					{
						strncpy(Splited_VTG[ii], p_start, p_end-p_start);			// przepisuj do poszcz. linii bufora dane spomiedzy dwoch przecinkow
						Splited_VTG[ii][p_end-p_start] = 0;							// zakoncz kazda linie znakiem \0
						ii++;														// inkrementuj numer linii
						p_start = p_end + 1;										// zacznij nastepne szukanie od kolejnego znaku po przecinku konczacym (zeby nie traktowal go jako nowego poczatkowego)
					}
					else															// koniec linii - nie znaleziono kolejnego przecinka		
					{						
						strncpy(Splited_VTG[ii], p_start, 15);						// przepisz koncowke znajdujaca sie w buforze (ostatnie okolo 20 znakow, nadmiar dla bezpieczenstwa)
						break;
					}
				}		
		}
		else if (String[a][3] == 'G' && String[a][4] == 'G' && String[a][5] == 'A' && flaga_gps_checksum[1] == 0xFF) // $GNGGA,220449.00,5332.41898,N,01432.44634,E,2,12,0.96,16.1,M,39.4,M,,0000*72
		{																											 //    0		 1			2	   3	  4	  5 6 7    8  9    10 11 1112   13
				while(1)
				{
					p_end = strchr(p_start, ',');
					if (p_end)
					{
						strncpy(Splited_GGA[ii], p_start, p_end-p_start); 
						Splited_GGA[ii][p_end-p_start] = 0; 
						ii++;
						p_start = p_end + 1;
					}
					else
					{
						// copy the last bit - might as well copy 20
						strncpy(Splited_GGA[ii], p_start, 20);
						break;
					}
				}		
		}
	}
	
	//if( flaga_gps_checksum[0] == 0xFF && flaga_gps_checksum[1] == 0xFF  ) uart2_puts("all checksum OK");
		
	// obrobka danych po splitowaniu ---------------------------------------------------------------------------------------------------------------------------------------------------
	
	Splited_GGA[1][6] = '\0'; Splited_GGA[1][7] = '\0'; Splited_GGA[1][8] = '\0'; // CzasUTC - skasowanie setnych sekund
	char *kropka = strchr(Splited_GGA[9],'.'); *kropka = '\0'; kropka++; *kropka = '\0'; // Wysokosc - kasowanie kropki i dziesietnych
	
	switch (Splited_GGA[6][0])
	{
	case '0':
		Fix[0] = '1';
		break;
	case '1':
		Fix[0] = '3';
		break;
	case '2':
		Fix[0] = '3';
		break;
	case '4':
		Fix[0] = '2';
		break;	
	case '5':
		Fix[0] = '2';
		break;
	case '6':
		Fix[0] = '1';
		break;
	default:
		Fix[0] = '\0';
		break;
	}
	
	char Szer_pocz[4] = {"0\0\0\0"};
	char Dlug_pocz[4] = {"0\0\0\0"};
	volatile static double Szerokosc_liczba = 0;
	volatile static double Dlugosc_liczba = 0;
	volatile static double Szer_liczba_pocz = 0;
	volatile static double Dlug_liczba_pocz = 0;
		
	Szer_pocz[0] = Splited_GGA[2][0];  Szer_pocz[1] = Splited_GGA[2][1];								  // przepisz tylko stopnie szerokosci
	Dlug_pocz[0] = Splited_GGA[4][0];  Dlug_pocz[1] = Splited_GGA[4][1];  Dlug_pocz[2]=Splited_GGA[4][2]; // przepisz tylko stopnie dlugosci
		
	for(uint8_t i = 0; i <= 7; i++) Splited_GGA[2][i] = Splited_GGA[2][i+2]; // przepisz z pozycji tylko dalsza czesc szerokosci geo
	for(uint8_t i = 0; i <= 7; i++) Splited_GGA[4][i] = Splited_GGA[4][i+3];  // przepisz sama dalsza czesc dlugosci geo
		
	Szer_liczba_pocz = atof(Szer_pocz);			// konwersja stringow na liczby zmiennoprzecinkowe
	Dlug_liczba_pocz = atof(Dlug_pocz);			//
	Szerokosc_liczba = atof(Splited_GGA[2]);	//
	Dlugosc_liczba = atof(Splited_GGA[4]);		//

	Szerokosc_liczba = Szerokosc_liczba/60.0 + Szer_liczba_pocz; // przeliczenie na pozycje w formacie decymentalnym
	Dlugosc_liczba = Dlugosc_liczba/60.0 + Dlug_liczba_pocz;

	dtostrf(Szerokosc_liczba,0,5,Splited_GGA[2]);		// konwersja spowrotem do stringa
	dtostrf(Dlugosc_liczba,0,5,Splited_GGA[4]);			// dla wygody uzyto zmiennych z poczatku funkcji [szerokosc i dlugosc]

	// zapisz ostatni poprawny zestaw danych----------------------------------------------------------------------------------------------------------------------------
	if (Fix[0] == '2' || Fix[0] == '3')
	{
		for(uint8_t i = 0; i < 10; i++) LastRamkaWysokosc[i] = '\0'; // czyszczenie [ lepiej byloby po prostu przepisac razem z NULL'em, ale bezpieczniej lecz wolniej jest czyscic calosc z ew. smieci ]
		for(uint8_t i = 0; i < 7; i++) LastRamkaCzasUTC[i] = '\0';
		for(uint8_t i = 0; i < 3; i++) LastRamkaSatelity[i] = '\0';
		for(uint8_t i = 0; i < 20; i++) LastRamkaSzerokosc[i] = '\0';
		for(uint8_t i = 0; i < 20; i++) LastRamkaDlugosc[i] = '\0';
		for(uint8_t i = 0; i < 15; i++) LastRamkaPredkosc[i] = '\0';
		
		for(uint8_t i = 0; Splited_GGA[1][i] != '\0' && i < 7; i++) LastRamkaCzasUTC[i] = Splited_GGA[1][i];
		for(uint8_t i = 0; Splited_GGA[2][i] != '\0' && i < 20; i++) LastRamkaSzerokosc[i] = Splited_GGA[2][i];
		for(uint8_t i = 0; Splited_GGA[4][i] != '\0' && i < 20; i++) LastRamkaDlugosc[i] = Splited_GGA[4][i];
		for(uint8_t i = 0; Splited_GGA[9][i] != '\0' && i < 10; i++) LastRamkaWysokosc[i] = Splited_GGA[9][i];
		for(uint8_t i = 0; Splited_GGA[7][i] != '\0' && i < 3; i++) LastRamkaSatelity[i] = Splited_GGA[7][i];
		for(uint8_t i = 0; Splited_VTG[7][i] != '\0' && i < 15; i++) LastRamkaPredkosc[i] = Splited_VTG[7][i];
		LastRamkaFix[0] = Fix[0];
	}
	else if (Fix[0] == '1')
	{
		for(uint8_t i = 0; i < 7; i++) LastRamkaCzasUTC[i] = '\0';
		for(uint8_t i = 0; i < 3; i++) LastRamkaSatelity[i] = '\0';
		
		for(uint8_t i = 0; Splited_GGA[1][i] != '\0' && i < 7; i++) LastRamkaCzasUTC[i] = Splited_GGA[1][i];
		for(uint8_t i = 0; Splited_GGA[7][i] != '\0' && i < 3; i++) LastRamkaSatelity[i] = Splited_GGA[7][i];
		//LastRamkaFix[0] = Fix[0];
	}

	// przeniesiona funkcja "oblicz wznoszenie" -------------------------------------------------------------------------------------------------------------------------
	int WysokoscInt = atoi(Splited_GGA[9]); // skonwertuj string do int'a
	int LastWysokoscInt = atoi(LastWysokosc); // inty pomocnicze do oblicze�
	int WznoszenieInt = 0;
		
	WznoszenieInt = (WysokoscInt - LastWysokoscInt) / 1; // r�nica w wysoko�ci dzielona przez czas mi�dzy pomiarami w sek.
	itoa(WznoszenieInt, Wznoszenie, 10); // konwersja gotowego wznoszenia
		
	for (uint8_t i=0; i<6; i++){ LastWysokosc[i] = Splited_GGA[9][i];} // przepisz obecn� wysoko�� jako poprzedni�

	// ( zerowanie glownego bufora po nadaniu ramki )	
}

void czysc_buf_GPS()				// dla pamieci >>> volatile char String[Liczba_Ramek_GPS+1][100]
{
	for (uint8_t a = 0; a < Liczba_Ramek_GPS+1; a++)
	{
		for (uint8_t b = 0; b < 100; b++)
		{
			String[a][b] = '\0';
		}
	}
	
	for (uint8_t c = 0; c < 12; c++)
	{
		for (uint8_t d = 0; d < 15; d++)
		{
			Splited_VTG[c][d] = '\0';
		}
	}
	
	for (uint8_t e = 0; e < 16; e++)
	{
		for (uint8_t f = 0; f < 20; f++)
		{
			Splited_GGA[e][f] = '\0';
		}
	}
	
	Fix[0] = '\0';
}


void Config_Ublox() // wrzuca tylko do ramu, ale nie pamieta nawet po chwilowym zaniku zasilania >> nie zachowuje sie jak opcja "save in BBR/flash memory"
{
	uart0_init(__UBRR_9200); // zmien baud rate na fabryczny w gps
	UCSR0B = (1<<TXEN0);	 // odblokuj nadawanie do gps
	const char disable_RMC[] = {0xB5,0x62,0x06,0x01,0x03,0x00,0xF0,0x04,0x00,0xFE,0x17}; // IN FUTURE: PRZEPISAC NA ZMIENNE W PAMIECI FLASH/EEPROM NA WYPADEK USZKODZENIA TEGO FRAGMENTU RAM W PRZYPADKU POTRZEBY UZYCIA
	const char disable_GSA[] = {0xB5,0x62,0x06,0x01,0x03,0x00,0xF0,0x02,0x00,0xFC,0x13};
	const char disable_GLL[] = {0xB5,0x62,0x06,0x01,0x03,0x00,0xF0,0x01,0x00,0xFB,0x11};
	const char disable_GSV[] = {0xB5,0x62,0x06,0x01,0x03,0x00,0xF0,0x03,0x00,0xFD,0x15};
	const char set_airborn[] = {0xB5,0x62,0x06,0x24,0x24,0x00,0xFF,0xFF,0x06,0x03,0x00,0x00,0x00,0x00,0x10,0x27,0x00,0x00,0x05,0x00,0xFA,0x00,0xFA,0x00,0x64,0x00,0x2C,0x01,0x00,0x3C,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x52,0xE8};
	const char set_19200_BR[] = {0xB5,0x62,0x06,0x00,0x14,0x00,0x01,0x00,0x00,0x00,0xD0,0x08,0x00,0x00,0x00,0x4B,0x00,0x00,0x07,0x00,0x03,0x00,0x00,0x00,0x00,0x00,0x48,0x57};
	const char set_save_conf[] = {0xB5,0x62,0x06,0x09,0x0D,0x00,0x00,0x00,0x00,0x00,0xFF,0xFF,0x00,0x00,0x00,0x00,0x00,0x00,0x07,0x21,0xAF};
	const char set_CFG_CFG[]= {0xB5,0x62,0x06,0x09,0x0C,0x00,0x00,0x00,0x00,0x00,0xFF,0xFF,0xFF,0xFF,0x00,0x00,0x00,0x00,0x17,0x75};

	uart0_puts_bytes(11, (char*)disable_RMC); _delay_ms(10); // przesylaj komendy i konfiguruj gps (przerwa czasowa na odpowiedz z gps)
	uart0_puts_bytes(11, (char*)disable_GSA); _delay_ms(10);
	uart0_puts_bytes(11, (char*)disable_GLL); _delay_ms(10);
	uart0_puts_bytes(11, (char*)disable_GSV); _delay_ms(10);
	uart0_puts_bytes(44, (char*)set_airborn); _delay_ms(10);
	uart0_puts_bytes(28, (char*)set_19200_BR); _delay_ms(10);
	uart0_init(__UBRR_0);   // zainicjalizuj port gps dla normalnej predkosci
	UCSR0B = (1<<TXEN0); // wlacz jeszcze nadawanie dla ostatniej komendy
	uart0_puts_bytes(21, (char*)set_save_conf); _delay_ms(10);  

	uart0_puts_bytes(20, (char*)set_CFG_CFG); _delay_ms(10);

	RESET_Watchdog(); // albo reset albo ponowna inicjalizacja uart, albo inicjalizacja glownego watku
}