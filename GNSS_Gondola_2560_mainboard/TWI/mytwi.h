#ifndef MYTWI_H_
#define MYTWI_H_


void I2C_Init();
void I2C_SetBusSpeed(uint16_t speed);
void I2C_SetCalculatedBusSpeed(uint16_t twbr, uint8_t prescal);
void I2C_Start();
  void I2C_Stop(); // static inline
  void I2C_WaitForComplete(); // static inline
  void I2C_WaitTillStopWasSent(); // static inline
void I2C_SendAddr(uint8_t address);
void I2C_SendStartAndSelect(uint8_t addr);
void I2C_SendByte(uint8_t byte);
uint8_t I2C_ReceiveData_NACK();
uint8_t I2C_ReceiveData_ACK();
  void I2C_SetError(uint8_t err); // inline





#endif /* MYTWI_H_ */
