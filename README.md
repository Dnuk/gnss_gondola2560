Firmware głównego komputera pokładowego uniwersalnej gondoli stratosferycznej V2.

Język C, środowisko Atmel Studio 6.x, mikrokontroler Atmega2560.

-- Wszystkie materiały udostępniane są na licencji open software/hardware do użytku niekomercyjnego

-- Uwaga - kompilacja w Atmel Studio 7+ uniemożliwi ponowne przeniesienie projektu do starszej wersji.
Może też wystąpić błąd z funkcją init();. Należy używać jej wtedy jako INLINE.

---------------------------------------------------------------------------------
Schematy, pliki PCB i dokumentacja (częściowo nieaktualna): 

https://bitbucket.org/Dnuk/gondolav2-hardware-repo/overview


---------------------------------------------------------------------------------
Oprogramowanie Stacji Naziemnej -> StratoTerminal v2.0 [nowy, mniej stabilny] By Fabian Oleksiuk
https://bitbucket.org/FabianoV/stratoterminal2

----------------------

StratoTerminal v1.1 -> [uzupełnić]

------------------------------------

Kontakt do nas oraz więcej materiałów tutaj:   https://www.facebook.com/DNFsystems/